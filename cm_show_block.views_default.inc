<?php
/**
 * @file
 * cm_show_block.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cm_show_block_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cm_show_block_view_shows';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'CM Show Block Shows';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Entity Reference View Widget Checkbox: Content */
  $handler->display->display_options['fields']['entityreference_view_widget']['id'] = 'entityreference_view_widget';
  $handler->display->display_options['fields']['entityreference_view_widget']['table'] = 'node';
  $handler->display->display_options['fields']['entityreference_view_widget']['field'] = 'entityreference_view_widget';
  $handler->display->display_options['fields']['entityreference_view_widget']['label'] = '';
  $handler->display->display_options['fields']['entityreference_view_widget']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['entityreference_view_widget']['ervw']['force_single'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: CM Show Duration */
  $handler->display->display_options['fields']['field_cm_show_duration']['id'] = 'field_cm_show_duration';
  $handler->display->display_options['fields']['field_cm_show_duration']['table'] = 'field_data_field_cm_show_duration';
  $handler->display->display_options['fields']['field_cm_show_duration']['field'] = 'field_cm_show_duration';
  $handler->display->display_options['fields']['field_cm_show_duration']['settings'] = array(
    'format' => 'h:mm:ss',
    'leading_zero' => 1,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cm_show' => 'cm_show',
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );
  /* Filter criterion: Content: CM Show Duration (field_cm_show_duration) */
  $handler->display->display_options['filters']['field_cm_show_duration_value']['id'] = 'field_cm_show_duration_value';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['table'] = 'field_data_field_cm_show_duration';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['field'] = 'field_cm_show_duration_value';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['operator'] = 'between';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['value']['input_format'] = 'h:mm:ss';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['operator_id'] = 'field_cm_show_duration_value_op';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['label'] = 'CM Show Duration';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['description'] = 'Enter the minimum and maximum length';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['operator'] = 'field_cm_show_duration_value_op';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['identifier'] = 'field_cm_show_duration_value';
  $handler->display->display_options['filters']['field_cm_show_duration_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );

  /* Display: Entity Reference View Widget */
  $handler = $view->new_display('entityreference_view_widget', 'Entity Reference View Widget', 'entityreference_view_widget_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $translatables['cm_show_block_view_shows'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Title'),
    t('CM Show Duration'),
    t('Enter the minimum and maximum length'),
    t('Entity Reference View Widget'),
  );
  $export['cm_show_block_view_shows'] = $view;

  return $export;
}
