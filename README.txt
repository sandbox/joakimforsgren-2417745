CONTENTS OF THIS FILE
---------------------

Introduction
You can create blocks of cm_show, each block can have multiple dates when the block will start broadcasting.
You can create cm_airing through this block
Each block keeps track of which cm_airings it has created, it makes it easy to remove all airings created by this particular block.
This module will make it much easier to create and keep track of cm_airing.

Requirements
cm_airing
cm_show
entityreference_view_widget

Installation
Enable the module

Maintainers
joakimforgren
